function search_cssactif(){ 
   window.open("http://www.css-actif.com/search?mode=searchbox&search_keywords="+document.search.query.value); 
}

function remplaceText(elt, match, newtext) {
   if($(elt).length) {
      return $(elt).html($(elt).html().replace(match, newtext));
   }
}

/* ************************************************
FONCTION DE SELECTION DU [CODE]
***************************************************/
function selectCode(a){a=a.parentNode.tagName==="B"?$(a).closest("table").find(".cont_code")[0]:$(a).closest("dl").find("code")[0];if(window.getSelection){var c=window.getSelection();if(c.setBaseAndExtent)c.setBaseAndExtent(a,0,a,a.innerText.length-1);else{window.opera&&a.innerHTML.substring(a.innerHTML.length-4)=="<BR>"&&(a.innerHTML+=" ");var b=document.createRange();b.selectNodeContents(a);c.removeAllRanges();c.addRange(b)}}else document.getSelection?(c=document.getSelection(),b=document.createRange(),b.selectNodeContents(a),
c.removeAllRanges(),c.addRange(b)):document.selection&&(b=document.body.createTextRange(),b.moveToElementText(a),b.select())};
 
$(function(){
    $("dl.codebox:not(.spoiler,.hidecode) dt").add($("div.cont_code").closest("table").find("span.genmed b")).append('<span onClick="selectCode(this)" class="selectCode">Sélectionner le contenu</span>');
});

$(function() {

	jQuery("#last-posted").html(jQuery("#comments_scroll_div").html());
	jQuery("#last-posted").find("marquee").css("width","auto");

	$('#emplacement_navigation').html($('#vraie_navigation_div').html());


	$('.boxgrid.captionfull').hover(function(){
		$(".cover", this).stop().show('slow');
	}, function() {
		$(".cover", this).stop().hide('slow', 'linear');
	});

		
	$('.avagrid.captionfull').hover(function(){
		$(".avacover", this).stop().show('slow');
	}, function() {
		$(".avacover", this).stop().hide('slow', 'linear');
	});

  // Compte le nombre de membres connectés au cours des 24 dernières heures et l'affiche
  var compte = 0;
  $("#qeel-24h a").each(function(index) {
      compte += 1;
  });
  $("#qeel-history h4").append(" (" + compte + " connectés)");




/* 
	//INITIALISATION DU DATEPICKER 
    $.datepicker.regional['fr'] = {
        closeText: 'Fermer',
        prevText: 'Précédent',
        nextText: 'Suivant',
        currentText: 'Aujourd\'hui',
        monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin',
            'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin',
            'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
        dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
        dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
        dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
        weekHeader: 'Sem.',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''};
    $.datepicker.setDefaults($.datepicker.regional['fr']);
	$( "#datetuto" ).datepicker({ dateFormat: "dd/mm/yy" });
*/

  /* NanoScroller */
  $(".nano").nanoScroller(
    { 
      preventPageScrolling: true,
       alwaysVisible: true 
    }
  );

/*
	// AFFICHAGE DU WIDGET DES DERNIERS TUTORIELS
	$.get(
	  '/h29-',
	   function (data) {
	       $('#widget-actu-tutos').html($(data).find('#slider-actu-tutos').html());
	   }
	);   
*/

	/* =================================================================
   == REMPLACEMENT DES PHRASES DU QEEL ================================
   ====================================================================*/
   if ($('#cssa-qeel').length ) {
       /* Nombre d'utilisateurs connectés (membres, invisibles, invités) */
      remplaceText('#qeel-nbonline', "Il y a en tout","<div>");
      remplaceText('#qeel-nbonline', "utilisateur en ligne :: ","</div>");
      remplaceText('#qeel-nbonline', "utilisateurs en ligne :: ","</div>");
      remplaceText('#qeel-nbonline', "et","<br />");
      remplaceText('#qeel-nbonline', "Enregistrés,","Membres<br />");
      remplaceText('#qeel-nbonline', "Enregistré,","Membre<br />");
      remplaceText('#qeel-nbonline', "Invité","Visiteur");

      /* Utilisateurs enregistrés */
      remplaceText('#qeel-logged',"Utilisateurs enregistrés :", "Sur le forum :")

      /* Nos membres ont posté un total de x messages */
      remplaceText('#qeel-nbposts',"Nos membres ont posté un total de", "");
      remplaceText('#qeel-nbposts',"messages", "");

      /* Nous avons x membres enregistrés */
      remplaceText('#qeel-nbusers',"Nous avons", "");
      remplaceText('#qeel-nbusers',"membres enregistrés", "");

      /* L'utilisateur enregistré le plus récent est xxxx */
      remplaceText('#qeel-lastuser',"L'utilisateur enregistré le plus récent est", "");

      /* Liste des utilisateurs connectés */
      remplaceText('#qeel-nbonline', "Utilisateurs enregistrés :","");

      /* Membres connectés au cours des 24 dernières heures : xxxxxx */
      remplaceText('#qeel-24h', "Membres connectés au cours des 24 dernières heures : ","");
      
      /* Liste des utilisateurs connectés sur la chatbox */
      remplaceText('#qeel-nbchatbox', "Il y a actuellement ","Il y a ");
      remplaceText('#qeel-nbchatbox', "utilisateur","membre");

      /* Le record du nombre d'utilisateurs en ligne est de X le DDD JJ MMM - HH:MM */
      remplaceText('#qeel-record', "Le record du nombre d'utilisateurs en ligne est de","<strong>Record de connexions :</strong> ");
      //remplaceText('#qeel-record', "le","connectés le");
   }     
});

/*
// What is that ?
$(window).load(function() {
   $('#slider').nivoSlider();
});
*/