$(document).ready(function(){

		$("#GenFormProjet").click(function(){
			/* recuperation des differentes variables */

			var nomforum = $("#nomforum").val();
			var urlforum = $("#urlforum").val();
			var btnforum = $("#btnforum").val();
			var descforum = $("#descforum").val();
			var contribforum = $("#contribforum").val();
			var fondforum = $("#fondforum").val();
			/*var gdlogo = $("#gdlogo").val();*/
			var apercufo = $("#apercufo").val();
			var actucode = $("#actucode").val();
			var realisations = $("#realisations").val();
			var ressources = $("#ressources").val();
			var butprojet = $("#butprojet").val();
			var butcterm = $("#butcterm").val();
			var inspiration = $("#inspiration").val();
			var commprj = $("#commprj").val();
			
			var listcomp = "";
			var idcomp = "";
			var unecomp = "";
			for (var i = 1; i<17; i++) {
				idcomp='listcomp_' + i;
				unecomp = $("label[for='"+ idcomp +"']").html();
				if($('#' + idcomp).attr('checked')) {
					if (i > 1) {listcomp += '\n';}
					listcomp += '[*]' + unecomp;
				}
				else { 
					if(i>1) {listcomp+= '\n';}
					listcomp += '[*][strike]'+unecomp+'[/strike]';
				}
			}

			if(!contribforum) contribforum = "Pas de contributeur";
			if(!commprj) commprj = "Rien à signaler";
		
			if (nomforum && urlforum && btnforum && descforum && fondforum && apercufo && actucode && realisations && ressources && butprojet && butcterm) {
				/* Création du code résultant */
			
					var code_resultant = '<h1>' + nomforum + '</h1><!--\n--><div class="cssaCommun cssaProjets"><h1>Fiche projet</h1>\n<h2>Information générales</h2><dl class="bloc infos-generales"><!--\n--><dt>URL :</dt><dd><!--\n\n-->' + urlforum + '<!--\n\n--></dd>\n<dt>Bouton :</dt><dd><!--\n\n-->[img]' + btnforum + '[/img]<!--\n\n--></dd>\n<dt>Description :</dt><dd><!--\n\n-->' + descforum + '<!--\n\n--></dd>\n<dt>Fondateur :</dt><dd><!--\n\n-->' + fondforum + '<!--\n\n--></dd>\n<dt>Contributeurs :</dt><dd><!--\n\n-->' + contribforum + '<!--\n\n--></dd></dl><!--\n--><h2>Mon projet</h2><div class="bloc"><!--\n-->\n<span class="formu">Mes compétences :</span>[list]' + listcomp + '[/list]<!--\n\n--></div><!--\n\n--><h2>Accomplissements</h2><div class="bloc"><!--\n\n -->[center]' + apercufo + '[/center]\n<span class="formu">Mon codage actuellement :</span> ' + actucode + '\n\n<span class="formu">Mes réalisations :</span> [list]' +realisations  + '[/list]<!--\n--><span class="formu">Ressources utilisées :</span> [list]' + ressources + '[/list]<!--\n--></div><!--\n\n--><h2>Objectifs</h2><div class="bloc"><!--\n--><span class="formu">But du projet :</span> ' + butprojet + '\n\n<span class="formu">Buts à court terme :</span> [list]' + butcterm + '[/list]<!--\n--><span class="formu">Sources d\'inspiration : </span> [list]' + inspiration + '[/list]<!--\n--><span class="formu">Commentaires, détails :</span> ' + commprj + '<!-- \n\n --></div></div>' ;
					var msg_generation = 'Vous avez généré le forumulaire pour soumettre votre projet, il ne vous reste qu\'à le copier et à le coller dans un nouveau sujet';
				

				/* On fait apparaitre le code à copier coller */      
				$("#generateur_resultat").fadeIn(400, function(){ /*on fait apparaitre le résultat*/
					$(this).find("textarea").text(code_resultant).select(); /*on affiche le contenu et on le sélectionne dans la foulée*/
					$(this).find("p.msg_resultant").html(msg_generation);
				});
			} else {
				alert("Vous devez remplir les champs obligatoires (précédés d'un astérisque *)!");
			}
			
			return false; /*on annule le comportement par défaut du bouton de validation*/	
		});	 
});